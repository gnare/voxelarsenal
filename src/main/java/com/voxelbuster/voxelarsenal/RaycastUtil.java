package com.voxelbuster.voxelarsenal;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.HashSet;
import java.util.stream.Collectors;

public class RaycastUtil {
    private static final HashSet<Material> transparentBlocks = new HashSet<>();

    static {
        transparentBlocks.addAll(Arrays.stream(Material.values()).filter(m -> m.isBlock() && !(m.isSolid() && m.isOccluding()))
                .collect(Collectors.toList()));
    }

    /**
     * Returns the entity the LivingEntity is looking at. Does not count entities obscured by an opaque block,
     * or entities outside the maximum distance, even if they are in the Iterable.
     *
     * @param entity LivingEntity
     * @param entities Iterable
     * @param maxDistance double
     * @return Entity
     */
    public static Entity getTargetEntity(@NotNull final LivingEntity entity, @NotNull final Iterable<Entity> entities, double maxDistance) {
        Vector direction = entity.getLocation().getDirection();
        return getTargetEntity(entity, entities, direction, maxDistance);
    }

    /**
     * Returns the entity in the direction from the LivingEntity. Does not count entities obscured by an opaque block,
     * or entities outside the maximum distance, even if they are in the Iterable.
     *
     * @param entity LivingEntity
     * @param entities Iterable
     * @param direction Vector
     * @param maxDistance double
     * @return Entity
     */
    public static @Nullable Entity getTargetEntity(@NotNull final LivingEntity entity, @NotNull final Iterable<Entity> entities, @NotNull Vector direction, double maxDistance) {
        Vector sourceLocation = entity.getLocation()
                .toVector();
        Vector nDirection = direction.normalize();
        for (final Entity other : entities) {

            double distance = sourceLocation.distance(other.getLocation().toVector());
            Vector targetVec = sourceLocation.clone().add(nDirection.clone().multiply(distance));

            if (targetVec.distance(other.getLocation().toVector()) <= 0.7d
                    && getTargetOpaqueBlock(entity, maxDistance).getLocation().toVector()
                    .distance(sourceLocation) > distance && distance <= maxDistance) {
                return other;
            }
        }
        return null;
    }

    /**
     * Returns the opaque block the entity is looking at. Treats all blocks as 1x1x1.
     *
     * @param entity LivingEntity
     * @param maxDistance double
     * @return Block
     */
    public static Block getTargetOpaqueBlock(LivingEntity entity, double maxDistance) {
        return entity.getTargetBlock(transparentBlocks, (int) maxDistance);
    }

    /**
     * Gets the location of the Entity or Block that the LivingEntity is looking at. If it is not looking at anything,
     * It returns the furthest Location it is allowed to check by maxDistance.
     *
     * @param entity LivingEntity
     * @param entities Iterable
     * @param maxDistance double
     * @return Location
     */
    public static @NotNull Location getHitLocation(@NotNull LivingEntity entity, @NotNull final Iterable<Entity> entities, double maxDistance) {
        return getHitLocation(entity, entities, entity.getLocation().getDirection(), maxDistance);
    }

    /**
     * Gets the location of the Entity or Block in the direction from the LivingEntity. If it is not looking at anything,
     * It returns the furthest Location it is allowed to check by maxDistance.
     *
     * @param entity LivingEntity
     * @param entities Iterable
     * @param direction Vector
     * @param maxDistance double
     * @return Location
     */
    public static @NotNull Location getHitLocation(@NotNull LivingEntity entity, @NotNull final Iterable<Entity> entities, @NotNull Vector direction, double maxDistance) {
        Entity targetEntity = getTargetEntity(entity, entities, direction, maxDistance);
        Block targetBlock = getTargetOpaqueBlock(entity, maxDistance);
        if (targetEntity  != null) {
            return targetEntity.getLocation();
        } else if (!transparentBlocks.contains(targetBlock.getType())) {
            return targetBlock.getLocation();
        } else {
            return entity.getLocation().toVector().add(entity.getLocation().getDirection().normalize()
                    .multiply(maxDistance)).toLocation(entity.getWorld());
        }
    }
}
