package com.voxelbuster.voxelarsenal.build;

import com.voxelbuster.voxelarsenal.VoxelArsenalPlugin;
import com.voxelbuster.voxelarsenal.objects.*;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Material;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Logger;

public final class PluginSetupUtil {
    public static void generateResources() {
        VoxelArsenalPlugin plugin = VoxelArsenalPlugin.getInstance();

        Logger log = plugin.getLogger();

        log.info("Generating plugin objects.");

        Ammo ammo_9mm = new Ammo("ammo_9mm", "9mm Ammo", Material.IRON_NUGGET, false, false, 0f);
        Ammo ammo_556 = new Ammo("ammo_556_nato", "5.56mm NATO Ammo", Material.GOLD_NUGGET, false, false, 0f);
        Ammo ammo_308win = new Ammo("ammo_308win", ".308 Winchester Ammo", Material.GHAST_TEAR, false, false, 0f);
        Ammo ammo_12ga = new Ammo("ammo_12ga", "12 Gauge Shotgun Ammo", Material.REDSTONE, false, false, 0f);
        Ammo ammo_rpg7 = new Ammo("ammo_rpg7", "RPG7 Ammo", Material.GUNPOWDER, false, false, 0f);

        Weapon weapon_m9 = new Weapon.Builder("weapon_9mm_pistol").setWeaponType(EnumWeaponType.PISTOL)
                .setAdsTime(0.15f).setAmmoId("ammo_9mm").setAmmoMaximum(13).setAttachmentMaximum(0)
                .setBaseSoundLevel(8f).setBaseZoom(0.5f).setDamage(4f).setFireDelay(0.4f).setHorizontalRecoil(0.12f)
                .setVerticalRecoil(0.14f).setNumProjectiles(1).setProjectileType(EnumProjectileType.BULLET)
                .setItem(Material.IRON_PICKAXE).setName("M9 Pistol").setReloadTime(0.8f).setSpreadFactor(0f)
                .setProjectileVelocity(ItemDataUtil.convertMetersPerSecond(380f)).build();

        Weapon weapon_ar15 = new Weapon.Builder("weapon_ar15_rifle").setWeaponType(EnumWeaponType.ASSAULT_RIFLE)
                .setAdsTime(0.15f).setAmmoId("ammo_556_nato").setAmmoMaximum(30).setAttachmentMaximum(0)
                .setBaseSoundLevel(12f).setBaseZoom(0.5f).setDamage(8f).setFireDelay(0.15f).setHorizontalRecoil(0.1f)
                .setVerticalRecoil(0.1f).setNumProjectiles(1).setProjectileType(EnumProjectileType.BULLET)
                .setItem(Material.IRON_HOE).setName("AR-15 Assault Rifle").setReloadTime(1.7f).setSpreadFactor(0f)
                .setProjectileVelocity(ItemDataUtil.convertMetersPerSecond(990f)).build();

        Weapon weapon_m700 = new Weapon.Builder("weapon_model700_rifle").setWeaponType(EnumWeaponType.ASSAULT_RIFLE)
                .setAdsTime(0.15f).setAmmoId("ammo_308win").setAmmoMaximum(6).setAttachmentMaximum(0)
                .setBaseSoundLevel(14f).setBaseZoom(0.7f).setDamage(17f).setFireDelay(1.6f).setHorizontalRecoil(0.15f)
                .setVerticalRecoil(0.22f).setNumProjectiles(1).setProjectileType(EnumProjectileType.BULLET)
                .setItem(Material.GOLDEN_HOE).setName("Remington Model 700").setReloadTime(5f).setSpreadFactor(0f)
                .setProjectileVelocity(ItemDataUtil.convertMetersPerSecond(900f)).build();

        Weapon weapon_sg = new Weapon.Builder("weapon_sg").setWeaponType(EnumWeaponType.SHOTGUN)
                .setAdsTime(0.15f).setAmmoId("ammo_12ga").setAmmoMaximum(2).setAttachmentMaximum(0)
                .setBaseSoundLevel(11f).setBaseZoom(0.4f).setDamage(2f).setFireDelay(1f).setHorizontalRecoil(-0.05f)
                .setVerticalRecoil(0.25f).setNumProjectiles(9).setProjectileType(EnumProjectileType.BULLET)
                .setItem(Material.IRON_HORSE_ARMOR).setName("Double-barrel Shotgun").setReloadTime(2.5f).setSpreadFactor(0.4f)
                .setProjectileVelocity(ItemDataUtil.convertMetersPerSecond(290f)).build();

        Weapon weapon_rpg7 = new Weapon.Builder("weapon_rpg7").setWeaponType(EnumWeaponType.ROCKET_LAUNCHER)
                .setAdsTime(0.15f).setAmmoId("ammo_rpg7").setAmmoMaximum(1).setAttachmentMaximum(0)
                .setBaseSoundLevel(10f).setBaseZoom(0.4f).setDamage(2f).setFireDelay(1f).setHorizontalRecoil(-0.4f)
                .setVerticalRecoil(0.4f).setNumProjectiles(1).setProjectileType(EnumProjectileType.ROCKET)
                .setItem(Material.IRON_SHOVEL).setName("RPG7 Rocket Launcher").setReloadTime(4.1f).setSpreadFactor(0f)
                .setProjectileVelocity(ItemDataUtil.convertMetersPerSecond(80f)).build();

        log.info("Writing plugin JSON files.");

        try {
            Files.createDirectories(Paths.get(plugin.getAmmoDirname()));
            Files.createDirectories(Paths.get(plugin.getWeaponsDirname()));
            ammo_9mm.toConfig(Paths.get(plugin.getAmmoDirname(), ammo_9mm.getId() + ".json").toFile());
            ammo_12ga.toConfig(Paths.get(plugin.getAmmoDirname(), ammo_12ga.getId() + ".json").toFile());
            ammo_308win.toConfig(Paths.get(plugin.getAmmoDirname(), ammo_308win.getId() + ".json").toFile());
            ammo_556.toConfig(Paths.get(plugin.getAmmoDirname(), ammo_556.getId() + ".json").toFile());
            ammo_rpg7.toConfig(Paths.get(plugin.getAmmoDirname(), ammo_rpg7.getId() + ".json").toFile());

            weapon_m9.toConfig(Paths.get(plugin.getWeaponsDirname(), weapon_m9.getId() + ".json").toFile());
            weapon_ar15.toConfig(Paths.get(plugin.getWeaponsDirname(), weapon_ar15.getId() + ".json").toFile());
            weapon_m700.toConfig(Paths.get(plugin.getWeaponsDirname(), weapon_m700.getId() + ".json").toFile());
            weapon_sg.toConfig(Paths.get(plugin.getWeaponsDirname(), weapon_sg.getId() + ".json").toFile());
            weapon_rpg7.toConfig(Paths.get(plugin.getWeaponsDirname(), weapon_rpg7.getId() + ".json").toFile());
        } catch (IOException e) {
            e.printStackTrace();
        }

        log.info("All resources generated.");
    }

    public static void copyResources() {
        VoxelArsenalPlugin plugin = VoxelArsenalPlugin.getInstance();
        Logger log = plugin.getLogger();

        log.info("Copying resources from plugin JAR. This will only occur once.");

        try {
            copyResourcesRecursively(plugin.getClass().getResource("/assets"), Paths.get(plugin.getDataFolder()
                    .toPath().toString(), "assets").toFile());
        } catch (IOException e) {
            log.severe("Failed to copy plugin resources!");
            e.printStackTrace();
        }
    }

    public static void copyResourcesRecursively(URL originUrl, File destination) throws IOException {
        URLConnection urlConnection = originUrl.openConnection();
        if (urlConnection instanceof JarURLConnection) {
            copyJarResourcesRecursively(destination, (JarURLConnection) urlConnection);
        /*} else if (urlConnection instanceof FileURLConnection) {
            FileUtils.copyDirectory(new File(originUrl.getPath()), destination);*/
        } else {
            throw new IllegalArgumentException("URLConnection[" + urlConnection.getClass().getSimpleName() +
                    "] is not a JAR connection type.");
        }
    }

    public static void copyJarResourcesRecursively(File destination,
                                                   JarURLConnection jarConnection) throws IOException {
        JarFile jarFile = jarConnection.getJarFile();
        for (Enumeration<JarEntry> it = jarFile.entries(); it.hasMoreElements(); ) {
            JarEntry entry = it.nextElement();
            if (entry.getName().startsWith(jarConnection.getEntryName())) {
                String fileName = StringUtils.removeStart(entry.getName(), jarConnection.getEntryName());
                if (!entry.isDirectory()) {
                    try (InputStream entryInputStream = jarFile.getInputStream(entry)) {
                        FileUtils.copyInputStreamToFile(entryInputStream, new File(destination, fileName));
                    }
                } else {
                    File dir = new File(destination, fileName);
                    if (!dir.exists()) {
                        dir.mkdir();
                    }
                }
            }
        }
    }
}
