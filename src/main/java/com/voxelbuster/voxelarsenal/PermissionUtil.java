package com.voxelbuster.voxelarsenal;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.permissions.Permission;
import org.bukkit.plugin.PluginManager;

import java.util.ArrayList;

public final class PermissionUtil {
    private static net.milkbowl.vault.permission.Permission permissionManager;

    public static void registerPermissions() {
        PluginManager pm = Bukkit.getPluginManager();
        ArrayList<Permission> permsList = new ArrayList<>();

        permsList.add(new Permission("voxelarsenal.reload"));
        permsList.add(new Permission("voxelarsenal.weapon.give"));
        permsList.add(new Permission("voxelarsenal.weapon.list"));
        permsList.add(new Permission("voxelarsenal.weapon.reload"));
        permsList.add(new Permission("voxelarsenal.weapon.setAmmo"));
        permsList.add(new Permission("voxelarsenal.weapon.freeReload"));
        permsList.add(new Permission("voxelarsenal.ammo.give"));
        permsList.add(new Permission("voxelarsenal.ammo.list"));
        permsList.add(new Permission("voxelarsenal.ammo.weapons"));


        permsList.forEach(pm::addPermission);
    }

    public static void setPermissionManager(net.milkbowl.vault.permission.Permission permissionManager) {
        com.voxelbuster.voxelarsenal.PermissionUtil.permissionManager = permissionManager;
    }

    public static boolean hasPermission(CommandSender sender, String node) {
        return sender.hasPermission(node) || (permissionManager != null && permissionManager.has(sender, node));
    }
}
