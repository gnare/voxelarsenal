package com.voxelbuster.voxelarsenal.dynamic.effects;

import de.slikey.effectlib.Effect;
import de.slikey.effectlib.EffectManager;
import de.slikey.effectlib.EffectType;
import org.apache.commons.lang.Validate;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.util.Vector;

public class SmokeTrailEffect extends Effect {
    public SmokeTrailEffect(EffectManager manager, Entity entity) {
        super(manager);
        this.type = EffectType.REPEATING;
        this.period = 1;
        this.setEntity(entity);
    }

    public SmokeTrailEffect(EffectManager manager, Location start, Location end) {
        super(manager);
        this.type = EffectType.INSTANT;
        this.setLocation(start);
        this.setTargetLocation(end);
    }

    @Override
    public void onRun() {
        if (getType() == EffectType.REPEATING) {
            Location location = this.getEntity().getLocation();
            // Create packet, to send to the player
            getEntity().getWorld().spawnParticle(Particle.FIREWORKS_SPARK, location, 1, 0d, 0d, 0d, 0d);
        } else if (getType() == EffectType.INSTANT) {
            // Calculate a line to target
            Vector start = getLocation().toVector();
            Vector end = getTarget().toVector();
            Vector direction = end.subtract(start).normalize();
            double distance = start.distance(end);
            int count = (int) (distance / 0.5);

            World w = getEntity().getWorld();
            drawLine(start.toLocation(w), end.toLocation(w), distance / count, Particle.FIREWORKS_SPARK);
        }
    }

    @Override
    protected void updateLocation() {
        super.updateLocation();
        if (getType() == EffectType.REPEATING) {
            Location location = this.getEntity().getLocation();
            // Create packet, to send to the player
            getEntity().getWorld().spawnParticle(Particle.FIREWORKS_SPARK, location, 1, 0d, 0d, 0d, 0d);
        }
    }

    public void drawLine(Location point1, Location point2, double space, Particle particle) {
        World world = point1.getWorld();
        Validate.isTrue(point2.getWorld().equals(world), "Lines cannot be in different worlds!");
        double distance = point1.distance(point2);
        Vector p1 = point1.toVector();
        Vector p2 = point2.toVector();
        Vector vector = p2.clone().subtract(p1).normalize().multiply(space);
        double length = 0;
        for (; length < distance; p1.add(vector)) {
            world.spawnParticle(particle, p1.getX(), p1.getY(), p1.getZ(), 1);
            length += space;
        }
    }
}
