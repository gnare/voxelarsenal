package com.voxelbuster.voxelarsenal.events;

import com.voxelbuster.voxelarsenal.objects.Weapon;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;

/**
 * An event that is fired when a player right-clicks a weapon in their hand (zooms or un-zooms).
 */
public class WeaponADSToggleEvent extends Event {
    private static final HandlerList handlers = new HandlerList();

    private final Player player;
    private final Weapon weapon;
    private final boolean aimingDownSights;

    public WeaponADSToggleEvent(@NotNull Player player, @NotNull Weapon weapon, boolean aimingDownSights) {
        this.player = player;
        this.weapon = weapon;
        this.aimingDownSights = aimingDownSights;
    }

    /**
     * Gets the player that triggered this event.
     * @return Player
     */
    @NotNull
    public Player getPlayer() {
        return player;
    }

    /**
     * Gets the weapon being aimed.
     * @return Weapon
     */
    @NotNull
    public Weapon getWeapon() {
        return weapon;
    }

    /**
     * Returns whether the player is ADSing (zoomed in)
     * @return true if ADSing, false if normal view
     */
    public boolean isAimingDownSights() {
        return aimingDownSights;
    }

    /**
     * Gets a list of Handlers for this event.
     */
    @Override
    public @NotNull HandlerList getHandlers() {
        return handlers;
    }
}
