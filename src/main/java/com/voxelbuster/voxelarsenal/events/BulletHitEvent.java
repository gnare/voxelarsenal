package com.voxelbuster.voxelarsenal.events;

import com.voxelbuster.voxelarsenal.objects.Weapon;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class BulletHitEvent extends Event {
    private static final HandlerList handlers = new HandlerList();

    private final Entity entityHit;
    private final Projectile projectile;
    private final Weapon weaponFired;
    private final double damageDealt;
    private final Player player;

    public BulletHitEvent(@Nullable Player player, @NotNull Entity entityHit, @NotNull Projectile projectile,
                          @Nullable Weapon weaponFired, double damageDealt) {
        this.entityHit = entityHit;
        this.projectile = projectile;
        this.weaponFired = weaponFired;
        this.damageDealt = damageDealt;
        this.player = player;
    }

    /**
     * Returns the entity hit by the projectile.
     */
    @NotNull
    public Entity getEntityHit() {
        return entityHit;
    }

    /**
     * Gets the projectile that caused this event.
     */
    @NotNull
    public Projectile getProjectile() {
        return projectile;
    }

    /**
     * Gets the weapon that caused this event.
     */
    @Nullable
    public Weapon getWeaponFired() {
        return weaponFired;
    }

    /**
     * Gets the damage dealt to the entity hit.
     */
    public double getDamageDealt() {
        return damageDealt;
    }

    /**
     * Gets the player that fired the projectile.
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * Gets a list of Handlers for this event.
     */
    @Override
    public @NotNull HandlerList getHandlers() {
        return handlers;
    }
}
