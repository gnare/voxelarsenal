package com.voxelbuster.voxelarsenal.events;

import com.voxelbuster.voxelarsenal.objects.Weapon;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.util.RayTraceResult;
import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Fired when a Player left-clicks (fires) a weapon that has ammo in it.
 */
public class WeaponFireEvent extends Event {
    private static final HandlerList handlerList = new HandlerList();

    private final Player player;
    private final Weapon weapon;
    private final List<Projectile> projectiles;
    private final List<RayTraceResult> rayTraceResults;


    public WeaponFireEvent(Player player, @NotNull Weapon weapon,
                           @NotNull List<Projectile> projectiles, @NotNull List<RayTraceResult> rayTraceResults) {
        this.player = player;
        this.weapon = weapon;
        this.projectiles = projectiles;
        this.rayTraceResults = rayTraceResults;
    }

    /**
     * Gets the player that caused this event.
     * @return
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * Gets the weapon that triggered this event.
     */
    @NotNull
    public Weapon getWeapon() {
        return weapon;
    }

    /**
     * Gets a list of projectiles created by the weapon being fired.
     */
    @NotNull
    public List<Projectile> getProjectiles() {
        return projectiles;
    }

    /**
     * Gets a list of RayTraceResults created by the weapon being fired.
     *
     * NOTE: This is currently not used. Hitscan does not currently use RayTraceResults.
     * @return Empty List
     */
    @NotNull
    public List<RayTraceResult> getRayTraceResults() {
        return rayTraceResults;
    }

    /**
     * Gets a list of Handlers for this event.
     */
    @Override
    public @NotNull HandlerList getHandlers() {
        return handlerList;
    }
}
