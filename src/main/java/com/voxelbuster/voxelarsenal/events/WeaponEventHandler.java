package com.voxelbuster.voxelarsenal.events;

import com.voxelbuster.voxelarsenal.ConfigManager;
import com.voxelbuster.voxelarsenal.VoxelArsenalPlugin;
import com.voxelbuster.voxelarsenal.WeaponManager;
import com.voxelbuster.voxelarsenal.objects.Weapon;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.persistence.PersistentDataType;

import java.util.Arrays;
import java.util.HashMap;

import static com.voxelbuster.voxelarsenal.objects.ItemDataUtil.ammoKey;
import static com.voxelbuster.voxelarsenal.objects.ItemDataUtil.weaponIdKey;

@SuppressWarnings("unused")
public class WeaponEventHandler implements Listener {

    private static final HashMap<EntityType, Double> headSizes = new HashMap<>();

    static {
        Arrays.stream(EntityType.values()).forEach(elt -> {
            switch (elt) {
                case BAT:
                case BEE:
                case PARROT:
                case PHANTOM:
                case CHICKEN:
                case CAT:
                case OCELOT:
                case TURTLE:
                case RABBIT:
                    headSizes.put(elt, 0.3d);
                    break;
                case ZOMBIE:
                case DROWNED:
                case HUSK:
                case ZOMBIE_VILLAGER:
                case PIG_ZOMBIE:
                case PANDA:
                case POLAR_BEAR:
                case GUARDIAN:
                case IRON_GOLEM:
                    headSizes.put(elt, 0.6d);
                    break;
                case PLAYER:
                case SKELETON:
                case WITCH:
                case BLAZE:
                case SPIDER:
                case STRAY:
                case VILLAGER:
                case VINDICATOR:
                case EVOKER:
                case SNOWMAN:
                case PILLAGER:
                case ILLUSIONER:
                case WANDERING_TRADER:
                    headSizes.put(elt, 0.5d);
                    break;
                case COW:
                case PIG:
                case SHEEP:
                case WOLF:
                case DOLPHIN:
                case ENDERMAN:
                case CAVE_SPIDER:
                case CREEPER:
                    headSizes.put(elt, 0.4d);
                    break;
                case FOX:
                    headSizes.put(elt, 0.35d);
                    break;
                case COD:
                case SALMON:
                case PUFFERFISH:
                case SILVERFISH:
                case VEX:
                case ENDERMITE:
                case TROPICAL_FISH:
                    headSizes.put(elt, 0.2d);
                    break;
                case HORSE:
                case LLAMA:
                case DONKEY:
                case WITHER_SKELETON:
                case GHAST:
                case MULE:
                case WITHER:
                case ZOMBIE_HORSE:
                case SKELETON_HORSE:
                    headSizes.put(elt, 0.7d);
                    break;
                case RAVAGER:
                case ELDER_GUARDIAN:
                    headSizes.put(elt, 0.8);
                    break;
                case ENDER_DRAGON:
                    headSizes.put(elt, 1.5d);
                    break;
                case GIANT:
                    headSizes.put(elt, 3.6d);
                    break;
                default:
                    headSizes.put(elt, 0d);
                    break;
            }
        });
    }

    private final VoxelArsenalPlugin plugin;
    private final WeaponManager weaponManager;
    private final ConfigManager.Config config;

    public WeaponEventHandler(VoxelArsenalPlugin voxelArsenalPlugin) {
        this.plugin = voxelArsenalPlugin;
        weaponManager = plugin.getWeaponManager();
        config = plugin.getConfigManager().getConfig();
    }

    @SuppressWarnings("unchecked")
    public static HashMap<EntityType, Double> getHeadSizeMap() {
        return (HashMap<EntityType, Double>) headSizes.clone();
    }

    /*@EventHandler // Replaced by temporary armor modification
    public void onEntityDamage(EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Projectile) {
            Projectile projectile = (Projectile) event.getDamager();
            if (projectile.getMetadata("name").get(0).asString().equals("Bullet") ||
                    projectile.getMetadata("name").get(0).asString().equals("Rocket")) {
                if (!plugin.getConfigManager().getConfig().doesArmorReduceBulletDamage()) {
                    event.setDamage(EntityDamageEvent.DamageModifier.ARMOR, 0);
                }
            }
        }
    }*/

    @EventHandler
    public void onItemUse(PlayerInteractEvent event) {
        Weapon[] weapons = weaponManager.getAllWeapons();
        ItemStack is = event.getItem();
        if (event.getAction().equals(Action.LEFT_CLICK_AIR) || event.getAction()
                .equals(Action.LEFT_CLICK_BLOCK)) { // Shoot
            for (Weapon w : weapons) {
                if (w.getId().equals(is.getItemMeta().getPersistentDataContainer()
                        .get(weaponIdKey, PersistentDataType.STRING))) {
                    if (event.getPlayer().getCooldown(event.getItem().getType()) > 0) {
                        event.setCancelled(true);
                        return;
                    }
                    Weapon playerWeaponFire = w.clone();
                    playerWeaponFire.setAmmo(event.getItem().getItemMeta().getPersistentDataContainer().get(
                            ammoKey, PersistentDataType.INTEGER));
                    playerWeaponFire.fire(event.getPlayer());
                    event.getPlayer().getInventory().setItemInMainHand(playerWeaponFire.getItemStack());
                    event.getPlayer()
                            .setCooldown(event.getItem().getType(), (int) (playerWeaponFire.getFireDelay() * 10f));
                }
            }
        } else if (event.getAction().equals(Action.RIGHT_CLICK_AIR) && event.getPlayer().isSneaking()) { // Reload
            for (Weapon w : weapons) {
                if (w.getId().equals(is.getItemMeta().getPersistentDataContainer()
                        .get(weaponIdKey, PersistentDataType.STRING))) {
                    if (event.getPlayer().getCooldown(event.getItem().getType()) > 0) {
                        event.setCancelled(true);
                        return;
                    }
                    Weapon playerWeaponFire = w.clone();
                    playerWeaponFire.setAmmo(event.getItem().getItemMeta().getPersistentDataContainer().get(
                            ammoKey, PersistentDataType.INTEGER));
                    playerWeaponFire.reload(event.getPlayer());
                    event.getPlayer().getInventory().setItemInMainHand(playerWeaponFire.getItemStack());
                    event.getPlayer()
                            .setCooldown(event.getItem().getType(), (int) (playerWeaponFire.getReloadTime() * 10));
                }
            }
        } else if (event.getAction().equals(Action.RIGHT_CLICK_AIR) && !event.getPlayer().isSneaking()) { // ADS
            Player player = event.getPlayer();
            for (Weapon w : weapons) {
                if (w.getId().equals(is.getItemMeta().getPersistentDataContainer()
                        .get(weaponIdKey, PersistentDataType.STRING))) {
                    Weapon playerWeaponFire = w.clone();
                    if (player.getCooldown(event.getItem().getType()) > 0) {
                        event.setCancelled(true);
                        return;
                    }
                    if (player.getWalkSpeed() >= 0.20f) {
                        player.setWalkSpeed(playerWeaponFire.getZoomFactor());
                        player
                                .setCooldown(event.getItem().getType(), (int) (playerWeaponFire.getAdsTime() * 10));
                        player.sendMessage(ChatColor.GREEN + "Scoped.");
                        plugin.getServer().getPluginManager().callEvent(new WeaponADSToggleEvent(player, w, true));
                    } else if (player.getWalkSpeed() <= 0.20f) {
                        player.setWalkSpeed(0.20f);
                        player.sendMessage(ChatColor.GREEN + "Unscoped.");
                        plugin.getServer().getPluginManager().callEvent(new WeaponADSToggleEvent(player, w, false));
                    }
                }
            }
        }
    }

    @EventHandler
    public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
        Projectile projectile = null;
        Player p = null;
        if (event.getDamager() instanceof Projectile) {
            projectile = (Projectile) event.getDamager();
            p = (projectile.getShooter() instanceof Player) ? (Player) projectile.getShooter() : null;
            if (projectile.getMetadata("name").get(0).asString().equals("Bullet")) {
                event.setCancelled(true);
                double damageDealt = 0d;
                if (event.getEntity() instanceof LivingEntity) {
                    LivingEntity le = (LivingEntity) event.getEntity();
                    if (!plugin.getConfigManager().getConfig().getBulletproofEntities()
                            .contains(le.getType().getKey().toString())) {
                        if (plugin.getConfigManager().getConfig().globalAllowHeadshots() && le instanceof Player &&
                                projectile.getLocation().getY() - le.getLocation().getY() > 1.35) {
                            damageDealt = projectile.getMetadata("damage").get(0).asDouble() *
                                    config.getGlobalHeadshotMultiplier(); // headshot

                            if (!plugin.getConfigManager().getConfig().doesArmorReduceBulletDamage()) {
                                double armor = le.getAttribute(Attribute.GENERIC_ARMOR).getBaseValue();
                                double armorToughness = le.getAttribute(Attribute.GENERIC_ARMOR_TOUGHNESS)
                                        .getBaseValue();
                                le.getAttribute(Attribute.GENERIC_ARMOR).setBaseValue(0d);
                                le.getAttribute(Attribute.GENERIC_ARMOR_TOUGHNESS).setBaseValue(0d);
                                le.damage(damageDealt, projectile);
                                le.getAttribute(Attribute.GENERIC_ARMOR).setBaseValue(armor);
                                le.getAttribute(Attribute.GENERIC_ARMOR_TOUGHNESS).setBaseValue(armorToughness);
                            } else {
                                le.damage(damageDealt, projectile);
                            }

                            plugin.getServer().getPluginManager()
                                    .callEvent(new HeadshotEvent(p, le, projectile, null, damageDealt));
                            if (p != null) {
                                p.playSound(p.getLocation(), Sound.ENTITY_ARROW_HIT_PLAYER, 1f, 1f);
                            }
                        } else {
                            damageDealt = projectile.getMetadata("damage").get(0).asDouble();

                            if (!plugin.getConfigManager().getConfig().doesArmorReduceBulletDamage()) {
                                double armor = le.getAttribute(Attribute.GENERIC_ARMOR).getBaseValue();
                                double armorToughness = le.getAttribute(Attribute.GENERIC_ARMOR_TOUGHNESS)
                                        .getBaseValue();
                                le.getAttribute(Attribute.GENERIC_ARMOR).setBaseValue(0d);
                                le.getAttribute(Attribute.GENERIC_ARMOR_TOUGHNESS).setBaseValue(0d);
                                le.damage(damageDealt, projectile);
                                le.getAttribute(Attribute.GENERIC_ARMOR).setBaseValue(armor);
                                le.getAttribute(Attribute.GENERIC_ARMOR_TOUGHNESS).setBaseValue(armorToughness);
                            } else {
                                le.damage(damageDealt, projectile);
                            }

                            if (p != null) {
                                p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_HURT, 1f, 1f);
                            }
                        }
                    }
                }
                plugin.getServer().getPluginManager()
                        .callEvent(new BulletHitEvent(p, event.getEntity(), projectile, null, damageDealt));
            }
        }
    }

    @SuppressWarnings("DuplicatedCode")
    @EventHandler
    public void onProjectileHit(ProjectileHitEvent event) {
        Projectile projectile = event.getEntity();

        if (projectile instanceof Arrow && projectile.hasMetadata("name") && projectile.getMetadata("name").get(0).asString().equals("Bullet")) {
            ((Arrow) projectile).setPickupStatus(AbstractArrow.PickupStatus.DISALLOWED);
            projectile.setTicksLived(1160); // Despawn after 2 seconds if arrow
        }

        Player p = null;
        if (projectile.getShooter() instanceof Player) {
            p = (Player) projectile.getShooter();
        }

        if (event.getHitEntity() != null && !event.getHitEntity().equals(projectile.getShooter())) {
            double damageDealt = 0d;
            if (projectile.hasMetadata("name") && projectile.getMetadata("name").get(0).asString().equals("Rocket")) {
                if (event.getHitEntity() != null && event.getHitEntity() instanceof LivingEntity) {
                    LivingEntity le = (LivingEntity) event.getHitEntity();
                    if (plugin.getConfigManager().getConfig().getBulletproofEntities()
                            .contains(le.getType().getKey().toString())) {

                        damageDealt = projectile.getMetadata("damage").get(0).asDouble();

                        if (!plugin.getConfigManager().getConfig().doesArmorReduceBulletDamage()) {
                            double armor = le.getAttribute(Attribute.GENERIC_ARMOR).getBaseValue();
                            double armorToughness = le.getAttribute(Attribute.GENERIC_ARMOR_TOUGHNESS).getBaseValue();
                            le.getAttribute(Attribute.GENERIC_ARMOR).setBaseValue(0d);
                            le.getAttribute(Attribute.GENERIC_ARMOR_TOUGHNESS).setBaseValue(0d);
                            le.damage(damageDealt, projectile);
                            le.getAttribute(Attribute.GENERIC_ARMOR).setBaseValue(armor);
                            le.getAttribute(Attribute.GENERIC_ARMOR_TOUGHNESS).setBaseValue(armorToughness);
                        } else {
                            le.damage(damageDealt, projectile);
                        }

                        if (p != null) {
                            p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_HURT, 1f, 1f);
                        }
                    }
                }
            }

            plugin.getServer().getPluginManager()
                    .callEvent(new BulletHitEvent(p, event.getHitEntity(), projectile, null, damageDealt));

        }

        if (projectile.hasMetadata("explode") && projectile.getMetadata("explode").get(0).asBoolean()) {
            projectile.getWorld().createExplosion(projectile.getLocation(),
                    projectile.getMetadata("explosionPower").get(0).asFloat(),
                    projectile.getMetadata("destroyBlocks").get(0).asBoolean() &&
                            plugin.getConfigManager().getConfig().doExplosionsDestroyBlocks());
        }
    }

    @EventHandler
    public void onItemClickedInInventory(InventoryClickEvent event) {
        InventoryAction action = event.getAction();
        if (action == InventoryAction.DROP_ONE_CURSOR || action == InventoryAction.DROP_ONE_SLOT ||
                action == InventoryAction.PICKUP_HALF || action == InventoryAction.PICKUP_SOME ||
                action == InventoryAction.PICKUP_ONE || action == InventoryAction.PLACE_ONE ||
                action == InventoryAction.PLACE_SOME) {
            Weapon[] weapons = weaponManager.getAllWeapons();
            for (Weapon w : weapons) {
                if (event.getCurrentItem().getType().equals(w.getItemStack().getType())) {
                    event.setCancelled(true); // Prevents splitting of weapon ItemStacks
                }
            }
        }
    }

    @EventHandler
    public void onProjectileLaunch(ProjectileLaunchEvent event) {

    }

    @EventHandler
    public void onBlockDestroyedByExplosion(EntityExplodeEvent event) {
        Entity e = event.getEntity();
        if (e.hasMetadata("name") && e.getMetadata("name").get(0).asString().equals("Rocket")) {
            if (e.hasMetadata("destroyBlocks") && !e.getMetadata("destroyBlocks").get(0).asBoolean()) {
                event.setCancelled(true);
            }
        }
    }
}
