package com.voxelbuster.voxelarsenal.events;

import com.voxelbuster.voxelarsenal.objects.Weapon;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.util.RayTraceResult;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Fired when a projectile hits the head of an entity
 */
public class HeadshotEvent extends Event {
    private static final HandlerList handlers = new HandlerList();

    private final Player player;
    private final LivingEntity hitEntity;
    private final Projectile projectile;
    private final Weapon weaponFired;
    private final double damageDealt;

    public HeadshotEvent(@Nullable Player player, @NotNull LivingEntity hitEntity, @Nullable Projectile projectile,
                         @Nullable Weapon weaponFired, double damageDealt) {
        if (projectile == null) {
            throw new IllegalArgumentException("Either a non-null Projectile or RayTraceResult must be passed!");
        }

        this.player = player;
        this.hitEntity = hitEntity;
        this.projectile = projectile;
        this.weaponFired = weaponFired;
        this.damageDealt = damageDealt;
    }

    /**
     * Gets the player that caused this event.
     */
    @Nullable
    public Player getPlayer() {
        return player;
    }

    /**
     * Gets the entity that was headshotted.
     */
    @NotNull
    public LivingEntity getHitEntity() {
        return hitEntity;
    }

    /**
     * Gets the projectile that caused this event.
     */
    @Nullable
    public Projectile getProjectile() {
        return projectile;
    }

    /**
     * Gets the weapon fired.
     */
    @Nullable
    public Weapon getWeaponFired() {
        return weaponFired;
    }

    /**
     * Gets the damage dealt to the hit entity.
     */
    public double getDamageDealt() {
        return damageDealt;
    }

    /**
     * Gets a list of Handlers for this event.
     */
    @Override
    public @NotNull HandlerList getHandlers() {
        return handlers;
    }
}
