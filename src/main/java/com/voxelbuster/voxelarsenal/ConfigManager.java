package com.voxelbuster.voxelarsenal;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import org.bukkit.entity.Player;

import java.io.*;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class ConfigManager {
    private Config config;
    private HashMap<Player, PlayerData> playerDataMap = new HashMap<>();
    private VoxelArsenalPlugin plugin;

    public ConfigManager(VoxelArsenalPlugin plugin) {
        this.plugin = plugin;
    }

    public boolean loadConfig(File configFile) throws IOException {
        if (generateConfig(configFile)) {
            return true;
        } else {
            Gson gson = new Gson();
            FileReader fr = new FileReader(configFile);
            try {
                this.config = gson.fromJson(fr, Config.class);
            } catch (JsonSyntaxException e) {
                plugin.getLogger().severe("Bad JSON in config, resetting.");
                e.printStackTrace();
                generateConfig(configFile);
                return false;
            }
            return false;
        }
    }

    public boolean generateConfig(File configFile) throws IOException {
        if (!configFile.exists()) {
            plugin.getDataFolder().mkdir();
            configFile.createNewFile();
            FileWriter fw = new FileWriter(configFile);
            this.config = new Config();
            Gson gson = new Gson();
            gson.toJson(config, fw);
            fw.close();
            return true;
        }

        return false;
    }

    public void saveConfig(File configFile) throws IOException {
        FileWriter fw = new FileWriter(configFile);
        Gson gson = new Gson();
        gson.toJson(config, fw);
        fw.close();
    }

    public void setPlayerData(Player p, PlayerData data) {
        this.playerDataMap.put(p, data);
    }

    public void saveAllPlayers(File playersDir) throws IOException {
        for (Player p : playerDataMap.keySet()) {
            savePlayer(p, playersDir);
        }
    }

    public boolean savePlayer(Player p, File playersDir) throws IOException {
        if (!playerDataMap.containsKey(p)) {
            return false;
        } else {
            Gson gson = new Gson();
            FileWriter fw = new FileWriter(
                    Paths.get(playersDir.toString(), p.getUniqueId().toString() + ".json").toString());
            gson.toJson(playerDataMap.get(p), fw);
            fw.close();
            return true;
        }
    }

    public void wipePlayers(File playersDir) throws IOException {
        Player[] loadedPlayers = getLoadedPlayers();
        unloadAllPlayers(playersDir);
        File oldPlayers = new File(playersDir.toPath().toString());
        oldPlayers.renameTo(new File(Paths.get(playersDir.getParentFile().toPath().toString(),
                "players.old").toString()));
        playersDir.mkdir();
        playerDataMap.clear();
        for (Player p : loadedPlayers) {
            loadPlayerData(p, playersDir);
        }
    }

    public Player[] getLoadedPlayers() {
        return playerDataMap.keySet().toArray(new Player[0]);
    }

    public void unloadAllPlayers(File playersDir) throws IOException {
        for (Player p : playerDataMap.keySet()) {
            unloadPlayer(p, playersDir);
        }
    }

    /**
     * Loads the PlayerData into the internal Map. Returns true if the file was created and false if it already exists.
     *
     * @param p
     * @param playersDir
     * @return
     * @throws IOException
     */
    public boolean loadPlayerData(Player p, File playersDir) throws IOException {
        if (!playersDir.isDirectory()) {
            playersDir.mkdirs();
            plugin.getLogger().warning("Created player data dir " + playersDir.getCanonicalPath());
        }

        File playerFile = new File(
                Paths.get(playersDir.toString(), p.getUniqueId().toString() + ".json").toString());
        if (!playerFile.exists()) {
            playerFile.createNewFile();
            PlayerData playerData = new PlayerData(p);
            playerDataMap.put(p, playerData);
            return true;
        } else {
            Gson gson = new Gson();
            FileReader fr = new FileReader(playerFile);
            try {
                PlayerData pd = gson.fromJson(fr, PlayerData.class);
                playerDataMap.put(p, pd);
            } catch (JsonSyntaxException e) {
                plugin.getLogger().severe("Bad JSON in config, resetting.");
                e.printStackTrace();
                playerDataMap.put(p, new PlayerData(p));
            }
            fr.close();
            return false;
        }
    }

    public boolean unloadPlayer(Player p, File playersDir) throws IOException {
        if (!playerDataMap.containsKey(p)) {
            return false;
        } else {
            Gson gson = new Gson();
            FileWriter fw = new FileWriter(
                    Paths.get(playersDir.toString(), p.getUniqueId().toString() + ".json").toString());
            gson.toJson(playerDataMap.remove(p), fw);
            fw.close();
            return true;
        }
    }

    public Config getConfig() {
        return config;
    }

    public PlayerData getPlayerData(Player p) {
        return playerDataMap.get(p);
    }

    public static class Config implements Serializable {
        private boolean explosionsDestroyBlocks = true;
        private boolean armorReducesBulletDamage = false;
        private boolean globalAllowHeadshots = true;
        private double globalHeadshotMultiplier = 2f;
        private boolean dropShellCasings = true;
        private boolean requireAmmoItemForReload = true;
        private boolean weaponStartWithFullAmmo = true;
        private String shellCasingItemId = "minecraft:bucket";
        private ArrayList<String> bulletproofEntities = new ArrayList<>();

        public boolean doExplosionsDestroyBlocks() {
            return explosionsDestroyBlocks;
        }

        public void setExplosionsDestroyBlocks(boolean explosionsDestroyBlocks) {
            this.explosionsDestroyBlocks = explosionsDestroyBlocks;
        }

        public boolean doesArmorReduceBulletDamage() {
            return armorReducesBulletDamage;
        }

        public void setArmorReducesBulletDamage(boolean armorReducesBulletDamage) {
            this.armorReducesBulletDamage = armorReducesBulletDamage;
        }

        public boolean globalAllowHeadshots() {
            return globalAllowHeadshots;
        }

        public void setGlobalAllowHeadshots(boolean globalAllowHeadshots) {
            this.globalAllowHeadshots = globalAllowHeadshots;
        }

        public double getGlobalHeadshotMultiplier() {
            return globalHeadshotMultiplier;
        }

        public void setGlobalHeadshotMultiplier(double globalHeadshotMultiplier) {
            this.globalHeadshotMultiplier = globalHeadshotMultiplier;
        }

        public boolean dropShellCasings() {
            return dropShellCasings;
        }

        public void setDropShellCasings(boolean dropShellCasings) {
            this.dropShellCasings = dropShellCasings;
        }

        public String getShellCasingItemId() {
            return shellCasingItemId;
        }

        public void setShellCasingItemId(String shellCasingItemId) {
            this.shellCasingItemId = shellCasingItemId;
        }

        public ArrayList<String> getBulletproofEntities() {
            return bulletproofEntities;
        }

        public void setBulletproofEntities(ArrayList<String> bulletproofEntities) {
            this.bulletproofEntities = bulletproofEntities;
        }

        public boolean requireAmmoItemForReload() {
            return requireAmmoItemForReload;
        }

        public void setRequireAmmoItemForReload(boolean requireAmmoItemForReload) {
            this.requireAmmoItemForReload = requireAmmoItemForReload;
        }

        public boolean weaponStartWithFullAmmo() {
            return weaponStartWithFullAmmo;
        }

        public void setWeaponStartWithFullAmmo(boolean weaponStartWithFullAmmo) {
            this.weaponStartWithFullAmmo = weaponStartWithFullAmmo;
        }
    }

    public static class PlayerData implements Serializable {
        private UUID uuid;
        private String username;

        public PlayerData(Player p) {
            this.uuid = p.getUniqueId();
            this.username = p.getName();
        }

        public UUID getUuid() {
            return uuid;
        }

        public void setUuid(UUID uuid) {
            this.uuid = uuid;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }
    }
}
