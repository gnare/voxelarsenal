package com.voxelbuster.voxelarsenal.objects;

import com.google.gson.Gson;
import com.voxelbuster.voxelarsenal.PermissionUtil;
import com.voxelbuster.voxelarsenal.RaycastUtil;
import com.voxelbuster.voxelarsenal.VoxelArsenalPlugin;
import com.voxelbuster.voxelarsenal.WeaponManager;
import com.voxelbuster.voxelarsenal.dynamic.effects.SmokeTrailEffect;
import com.voxelbuster.voxelarsenal.events.*;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.util.RayTraceResult;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static com.voxelbuster.voxelarsenal.objects.ItemDataUtil.*;

public class Weapon implements Cloneable {
    private final float damage, projectileVelocity, reloadTime, adsTime, fireDelay, baseSoundLevel, baseZoom,
            spreadFactor, horizontalRecoil, verticalRecoil;
    private final int ammoMaximum, attachmentMaximum, numProjectiles;
    private final String ammoID;
    private final EnumWeaponType weaponType;
    private final EnumProjectileType projectileType;
    private final String displayName;

    private final String id;
    private final Material item;
    private transient WeaponManager weaponManager = WeaponManager.getInstance();
    private transient VoxelArsenalPlugin plugin = VoxelArsenalPlugin.getInstance();
    private transient int ammoCount = 0;
    private transient WeaponAttachment[] attachments;

    private Weapon(float damage, float projectileVelocity, float reloadTime, float adsTime, float spreadFactor,
                   float horizontalRecoil, float verticalRecoil, int ammoMaximum,
                   int attachmentMaximum, int numProjectiles, @NotNull String ammoId, EnumWeaponType weaponType,
                   @NotNull String displayName, Material item, float fireDelay, float baseSoundLevel, float baseZoom,
                   EnumProjectileType projectileType, @NotNull String id) {
        this.damage = damage;
        this.projectileVelocity = projectileVelocity;
        this.reloadTime = reloadTime;
        this.adsTime = adsTime;
        this.spreadFactor = spreadFactor;
        this.horizontalRecoil = horizontalRecoil;
        this.verticalRecoil = verticalRecoil;
        this.ammoMaximum = ammoMaximum;
        this.attachmentMaximum = attachmentMaximum;
        this.numProjectiles = numProjectiles;
        this.ammoID = ammoId;
        this.weaponType = weaponType;
        this.displayName = displayName;
        this.item = item;
        this.fireDelay = fireDelay;
        this.baseSoundLevel = baseSoundLevel;
        this.baseZoom = baseZoom;
        this.projectileType = projectileType;
        this.id = id;

        this.attachments = new WeaponAttachment[this.attachmentMaximum];

        if (plugin.getConfigManager().getConfig().weaponStartWithFullAmmo()) {
            ammoCount = getActualAmmoMaximum();
        }
    }

    public int getActualAmmoMaximum() {
        int currentMax = ammoMaximum;
        for (WeaponAttachment attachment : attachments) {
            currentMax = Math.max(currentMax, attachment.getAmmo());
        }
        return currentMax;
    }

    public static @NotNull Weapon fromConfig(File jsonFile) throws IOException {
        Gson gson = new Gson();
        FileReader fr = new FileReader(jsonFile);
        Weapon obj = gson.fromJson(fr, Weapon.class);
        obj.attachments = new WeaponAttachment[obj.attachmentMaximum];
        obj.weaponManager = WeaponManager.getInstance();
        obj.plugin = VoxelArsenalPlugin.getInstance();
        fr.close();
        return obj;
    }

    @Nullable
    public static Weapon fromItemStack(@NotNull ItemStack is) {
        ItemMeta meta = is.getItemMeta();
        assert meta != null;
        PersistentDataContainer dataContainer = meta.getPersistentDataContainer();
        int ammoCount = dataContainer.get(ammoKey, PersistentDataType.INTEGER);
//        int ammoMax = dataContainer.get(maxAmmoKey, PersistentDataType.INTEGER); // will be used in later versions

        Weapon result = null;
        for (Weapon w : WeaponManager.getInstance().getAllWeapons()) {
            if (w.getId().equals(is.getItemMeta().getPersistentDataContainer()
                    .get(weaponIdKey, PersistentDataType.STRING))) {
                result = w.clone();
            }
        }

        if (result != null) {
            result.setAmmo(ammoCount);
        }

        return result;
    }

    public ItemStack getItemStack() {
        ItemStack is = new ItemStack(item);
        if (ammoCount == 0) {
            is.setAmount(1);
        } else {
            is.setAmount(ammoCount);
        }

        ItemMeta meta = is.getItemMeta();
        assert meta != null;
        PersistentDataContainer dataContainer = meta.getPersistentDataContainer();
        dataContainer.set(ammoKey, PersistentDataType.INTEGER, ammoCount);
        dataContainer.set(maxAmmoKey, PersistentDataType.INTEGER, ammoMaximum);
        dataContainer.set(weaponIdKey, PersistentDataType.STRING, id);
        meta.setDisplayName(displayName);
        meta.setUnbreakable(true);

        ArrayList<String> loreList = new ArrayList<>();
        loreList.add(weaponManager.getAmmoById(ammoID).getDisplayName());
        loreList.add("Ammo: " + ammoCount + " / " + getActualAmmoMaximum());
        for (WeaponAttachment attachment : attachments) {
            loreList.add(attachment.getDisplayName());
        }
        meta.setLore(loreList);

        is.setItemMeta(meta);

        return is;
    }

    @SuppressWarnings("MethodDoesntCallSuperMethod")
    @Override
    public Weapon clone() {
        Weapon objClone = new Weapon(damage, projectileVelocity, reloadTime, adsTime, spreadFactor, horizontalRecoil,
                verticalRecoil, ammoMaximum, attachmentMaximum, numProjectiles, ammoID, weaponType, displayName, item,
                fireDelay, baseSoundLevel, baseZoom, projectileType, id);
        objClone.setAmmo(this.ammoCount);
        for (WeaponAttachment wa : this.attachments) {
            objClone.addAttachment(wa);
        }
        return objClone;
    }

    public boolean addAttachment(WeaponAttachment attachment) {
        List<WeaponAttachment> oldAttachments = Arrays.asList(attachments);
        int slot = -1;
        for (int i = 0; i < attachments.length; i++) {
            if (attachments[i] == null) {
                slot = i;
                break;
            }
        }

        if (slot > -1) {
            attachments[slot] = attachment;
            plugin.getServer().getPluginManager()
                    .callEvent(new WeaponAttachmentChangeEvent(this, oldAttachments, Arrays.asList(attachments)));
            return true;
        } else {
            return false;
        }
    }

    public int getBaseAmmoMaximum() {
        return this.ammoMaximum;
    }

    public int reload(Player p) {
        if (plugin.getConfigManager().getConfig().requireAmmoItemForReload() &&
                !PermissionUtil.hasPermission(p, "voxelarsenal.weapon.freeReload")) {
            Ammo a = weaponManager.getAmmoById(ammoID);
            AtomicInteger availableCount = new AtomicInteger(ammoCount);
            if (a != null) {
                ItemStack ammoItemStack = a.getItemStack();
                p.getInventory().iterator().forEachRemaining(is -> {
                    if (is.isSimilar(ammoItemStack)) {
                        availableCount.addAndGet(is.getAmount());
                    }
                });
            }
            plugin.getServer().getPluginManager().callEvent(new WeaponReloadEvent(p, this));

            int newAmmoCount = Math.min(getActualAmmoMaximum(), availableCount.get());
            this.setAmmo(newAmmoCount);
            return newAmmoCount;
        } else {
            this.setAmmo(getActualAmmoMaximum());
            return getActualAmmoMaximum();
        }
    }

    public void setAmmo(int ammo) {
        this.ammoCount = ammo;
    }

    @SuppressWarnings("DuplicatedCode")
    public List<Projectile> fire(Player player) {
        Vector playerDirection = player.getLocation().getDirection();
        ArrayList<Projectile> projectiles = new ArrayList<>();
        ArrayList<RayTraceResult> rayTraceResults = new ArrayList<>();
        if (ammoCount > 0) {
            if (plugin.getConfigManager().getConfig().dropShellCasings()) {
                ejectCasing(player, 0.3d);
            }

            ammoCount--;

            Location origin = player.getLocation();

            float linearVelocity = projectileVelocity;
            for (WeaponAttachment attachment : attachments) {
                linearVelocity *= attachment.getVelocityMultiplier();
            }

            for (int i = 0; i < numProjectiles; i++) {
                Projectile projectile = null;
                if (projectileType == EnumProjectileType.BULLET) {
                    projectile = player.getWorld()
                            .spawnArrow(player.getLocation(), player.getEyeLocation().getDirection(), linearVelocity,
                                    spreadFactor);
                    ((Arrow) projectile).setPickupStatus(AbstractArrow.PickupStatus.DISALLOWED);
                    projectile.setSilent(true);

                    // Set projectile data for events
                    projectile.setMetadata("name", new FixedMetadataValue(VoxelArsenalPlugin.getInstance(), "Bullet"));
                    projectile.setMetadata("damage", new FixedMetadataValue(VoxelArsenalPlugin.getInstance(), damage));
                    projectile.setShooter(player);

                    SmokeTrailEffect ste = new SmokeTrailEffect(VoxelArsenalPlugin.getInstance().getEffectManager(),
                            projectile);
                    VoxelArsenalPlugin.getInstance().getEffectManager().start(ste);
                } else if (projectileType == EnumProjectileType.ROCKET) {
                    projectile = player.launchProjectile(SizedFireball.class);
                    Ammo a = weaponManager.getAmmoById(ammoID);
                    if (a != null) {
                        SmallFireball fireball = (SmallFireball) projectile;
                        fireball.setYield(a.getExplosionPower());
                        fireball.setIsIncendiary(true);
                    }
                    projectile.setVelocity(projectile.getVelocity().multiply(linearVelocity));
                    Vector velocity = projectile.getVelocity().clone();
                    Vector direction = playerDirection.clone();
                    velocity.rotateAroundAxis(direction.crossProduct(new Vector(0, 1, 0)),
                            getSpreadVelocityModifier(spreadFactor));
                    velocity.rotateAroundAxis(direction, new Random().nextDouble() * 360d);
                    projectile.setSilent(true);
                    projectile.setGravity(false);

                    // Set projectile data for events
                    projectile.setMetadata("name", new FixedMetadataValue(VoxelArsenalPlugin.getInstance(), "Rocket"));
                    projectile.setMetadata("damage", new FixedMetadataValue(VoxelArsenalPlugin.getInstance(), damage));
                    if (a != null) {
                        projectile.setMetadata("explode",
                                new FixedMetadataValue(VoxelArsenalPlugin.getInstance(), a.isExplosive()));
                        projectile.setMetadata("explosionPower",
                                new FixedMetadataValue(VoxelArsenalPlugin.getInstance(), a.getExplosionPower()));
                        projectile.setMetadata("destroyBlocks", new FixedMetadataValue(VoxelArsenalPlugin.getInstance(),
                                a.doesExplosionDestroyBlocks()));
                    }
                    projectile.setShooter(player);

                    SmokeTrailEffect ste = new SmokeTrailEffect(VoxelArsenalPlugin.getInstance().getEffectManager(),
                            projectile);
                    VoxelArsenalPlugin.getInstance().getEffectManager().start(ste);
                } else if (projectileType == EnumProjectileType.HITSCAN) {
                    Vector direction = playerDirection.clone();
                    //direction.rotateAroundAxis(direction.crossProduct(new Vector(0, 1, 0)),
                    // getSpreadVelocityModifier(spreadFactor));
                    //direction.rotateAroundAxis(player.getLocation().getDirection(), new Random().nextDouble() * 360d);
                    //direction.normalize();

                    List<Entity> nearbyEntities = player.getNearbyEntities(200f, 200f, 200f).stream()
                            .filter(e -> e instanceof LivingEntity).collect(Collectors.toList());
                    Entity e = RaycastUtil.getTargetEntity(player, nearbyEntities, direction, 200d);

                    Vector hitLocation = RaycastUtil.getHitLocation(player, nearbyEntities, direction, 200d)
                            .toVector();
                    if (e instanceof LivingEntity) {
                        LivingEntity le = (LivingEntity) e;
                        if (!plugin.getConfigManager().getConfig().getBulletproofEntities()
                                .contains(le.getType().getKey().toString())) {
                            double headSize = WeaponEventHandler.getHeadSizeMap().get(e.getType());
                            if (plugin.getConfigManager().getConfig().globalAllowHeadshots() &&
                                    le.getEyeLocation().toVector().distance(hitLocation) < headSize / 2) {
                                double damageDealt = getDamage() * plugin.getConfigManager().getConfig()
                                        .getGlobalHeadshotMultiplier();

                                if (!plugin.getConfigManager().getConfig().doesArmorReduceBulletDamage()) {
                                    double armor = le.getAttribute(Attribute.GENERIC_ARMOR).getBaseValue();
                                    double armorToughness = le.getAttribute(Attribute.GENERIC_ARMOR_TOUGHNESS)
                                            .getBaseValue();
                                    le.getAttribute(Attribute.GENERIC_ARMOR).setBaseValue(0d);
                                    le.getAttribute(Attribute.GENERIC_ARMOR_TOUGHNESS).setBaseValue(0d);
                                    le.damage(damageDealt, player);
                                    le.getAttribute(Attribute.GENERIC_ARMOR).setBaseValue(armor);
                                    le.getAttribute(Attribute.GENERIC_ARMOR_TOUGHNESS).setBaseValue(armorToughness);
                                } else {
                                    le.damage(damageDealt, player);
                                }

                                plugin.getServer().getPluginManager()
                                        .callEvent(new RayTraceHitEvent(player, le,
                                                this, damageDealt));
                                plugin.getServer().getPluginManager().callEvent(
                                        new HeadshotEvent(player, le, null, this, damageDealt));
                            } else {
                                double damageDealt = getDamage();

                                if (!plugin.getConfigManager().getConfig().doesArmorReduceBulletDamage()) {
                                    double armor = le.getAttribute(Attribute.GENERIC_ARMOR).getBaseValue();
                                    double armorToughness = le.getAttribute(Attribute.GENERIC_ARMOR_TOUGHNESS)
                                            .getBaseValue();
                                    le.getAttribute(Attribute.GENERIC_ARMOR).setBaseValue(0d);
                                    le.getAttribute(Attribute.GENERIC_ARMOR_TOUGHNESS).setBaseValue(0d);
                                    le.damage(damageDealt, player);
                                    le.getAttribute(Attribute.GENERIC_ARMOR).setBaseValue(armor);
                                    le.getAttribute(Attribute.GENERIC_ARMOR_TOUGHNESS).setBaseValue(armorToughness);
                                } else {
                                    le.damage(damageDealt, player);
                                }

                                plugin.getServer().getPluginManager()
                                        .callEvent(new RayTraceHitEvent(player, le, this, damageDealt));
                            }
                        }
                        SmokeTrailEffect ste = new SmokeTrailEffect(
                                VoxelArsenalPlugin.getInstance().getEffectManager(),
                                player.getEyeLocation(), le.getLocation());
                        VoxelArsenalPlugin.getInstance().getEffectManager().start(ste);
                    } else {
                        SmokeTrailEffect ste = new SmokeTrailEffect(
                                VoxelArsenalPlugin.getInstance().getEffectManager(),
                                player.getEyeLocation(), hitLocation.toLocation(player.getWorld()));
                        VoxelArsenalPlugin.getInstance().getEffectManager().start(ste);
                    }
                }

                // Calculate shot volume and pitch
                float volume = baseSoundLevel;
                for (WeaponAttachment attachment : attachments) {
                    volume *= attachment.getSoundMultiplier();
                }
                float pitch = (float) (baseSoundLevel / Math.sqrt(volume)); // Suppressed shots are higher pitched
                player.getWorld().playSound(origin, Sound.ENTITY_BLAZE_SHOOT, volume, pitch);
                projectiles.add(projectile);
            }

            if (ammoCount == 0) {
                player.sendMessage(ChatColor.RED + "Shift-Rightclick to reload!");
                player.getWorld().playSound(player.getLocation(), Sound.BLOCK_TRIPWIRE_CLICK_ON, 1f, 1f);
                plugin.getServer().getPluginManager().callEvent(new WeaponOutOfAmmoEvent(this, player));
            }

            plugin.getServer().getPluginManager().callEvent(
                    new WeaponFireEvent(player, this, projectiles, rayTraceResults));
        } else {
            player.sendMessage(ChatColor.RED + "Shift-Rightclick to reload!");
            player.getWorld().playSound(player.getLocation(), Sound.BLOCK_TRIPWIRE_CLICK_ON, 1f, 1f);
            plugin.getServer().getPluginManager().callEvent(new WeaponOutOfAmmoEvent(this, player));
        }


        playerDirection.rotateAroundAxis(new Vector(0, -1, 0),
                horizontalRecoil * Math.random() * ((Math.random() < 0.5d) ? -1 : 1));
        Vector transformAxis = playerDirection.crossProduct(new Vector(0, -1, 0));
        playerDirection.rotateAroundAxis(transformAxis, verticalRecoil * Math.random());

        return projectiles;
    }

    public static double getSpreadVelocityModifier(float spreadFactor) {
        return Math.random() * spreadFactor;
    }

    public double getDamage() {
        return this.damage;
    }

    protected static Item ejectCasing(Player player, double shellVelocityMultiplier) {
        Item i = player.getWorld().dropItem(player.getLocation(), new ItemStack(Material.matchMaterial(
                VoxelArsenalPlugin.getInstance().getConfigManager().getConfig().getShellCasingItemId())));
        i.setPickupDelay(6000);
        i.setTicksLived(4800); // make items only last 1 minute

        Vector direction = player.getLocation().getDirection();
        // shell flies perpendicular to the ground and the barrel
        Vector velocity = direction.crossProduct(new Vector(0, -1, 0).multiply(shellVelocityMultiplier));
        i.setVelocity(velocity);
        return i;
    }

    public boolean setAttachment(int index, WeaponAttachment attachment) {
        List<WeaponAttachment> oldAttachments = Arrays.asList(attachments);
        if (index >= attachments.length) {
            return false;
        } else {
            attachments[index] = attachment;
            plugin.getServer().getPluginManager()
                    .callEvent(new WeaponAttachmentChangeEvent(this, oldAttachments, Arrays.asList(attachments)));
            return true;
        }
    }

    public WeaponAttachment getAttachment(int index) {
        return attachments[index];
    }

    public boolean removeAttachment(int index) {
        List<WeaponAttachment> oldAttachments = Arrays.asList(attachments);
        if (index >= attachments.length) {
            return false;
        } else {
            attachments[index] = null;
            plugin.getServer().getPluginManager()
                    .callEvent(new WeaponAttachmentChangeEvent(this, oldAttachments, Arrays.asList(attachments)));
            return true;
        }
    }

    public float getZoomFactor() {
        float zoom = -baseZoom;
        for (int i = 0; i < attachments.length; i++) {
            if (attachments[i] != null) {
                zoom = Math.min(zoom, -attachments[i].getZoomFactor());
            }
        }
        return zoom;
    }

    public float getAdsTime() {
        return adsTime;
    }

    public String getId() {
        return id;
    }

    public float getFireDelay() {
        return fireDelay;
    }

    public float getReloadTime() {
        return reloadTime;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getAmmoID() {
        return ammoID;
    }

    public void toConfig(File jsonFile) throws IOException {
        Gson gson = new Gson();
        FileWriter fw = new FileWriter(jsonFile);
        gson.toJson(this, fw);
        fw.close();
    }

    public static class Builder {
        private float damage = 4f;
        private float projectileVelocity = 10f;
        private float reloadTime = 1f;
        private float adsTime = 0.1f;
        private float spreadFactor = 0f;
        private int ammoMaximum = 1;
        private int attachmentMaximum = 0;
        private String ammoId = "";
        private EnumWeaponType weaponType = EnumWeaponType.PISTOL;
        private String name = "Weapon";
        private Material item = Material.STONE;
        private float fireDelay = 1f;
        private float baseSoundLevel = 8f;
        private float baseZoom = 0.15f;
        private EnumProjectileType projectileType;
        private String id;
        private int numProjectiles = 1;
        private float horizontalRecoil = 0f;
        private float verticalRecoil = 0f;

        public Builder(String id) {
            this.id = id;
        }

        public Builder setDamage(float damage) {
            this.damage = damage;
            return this;
        }

        public Builder setProjectileType(EnumProjectileType projectileType) {
            this.projectileType = projectileType;
            return this;
        }

        public Builder setProjectileVelocity(float projectileVelocity) {
            this.projectileVelocity = projectileVelocity;
            return this;
        }

        public Builder setReloadTime(float reloadTime) {
            this.reloadTime = reloadTime;
            return this;
        }

        public Builder setAdsTime(float adsTime) {
            this.adsTime = adsTime;
            return this;
        }

        public Builder setAmmoMaximum(int ammoMaximum) {
            this.ammoMaximum = ammoMaximum;
            return this;
        }

        public Builder setAttachmentMaximum(int attachmentMaximum) {
            this.attachmentMaximum = attachmentMaximum;
            return this;
        }

        public Builder setAmmoId(@NotNull String ammoId) {
            this.ammoId = ammoId;
            return this;
        }

        public Builder setWeaponType(EnumWeaponType weaponType) {
            this.weaponType = weaponType;
            return this;
        }

        public Builder setName(@NotNull String name) {
            this.name = name;
            return this;
        }

        public Builder setItem(Material item) {
            this.item = item;
            return this;
        }

        public Builder setFireDelay(float fireDelay) {
            this.fireDelay = fireDelay;
            return this;
        }

        public Builder setBaseSoundLevel(float baseSoundLevel) {
            this.baseSoundLevel = baseSoundLevel;
            return this;
        }

        public Builder setBaseZoom(float baseZoom) {
            this.baseZoom = baseZoom;
            return this;
        }

        public Weapon build() {
            return new Weapon(damage, projectileVelocity, reloadTime, adsTime, spreadFactor, horizontalRecoil,
                    verticalRecoil, ammoMaximum, attachmentMaximum,
                    numProjectiles, ammoId,
                    weaponType, name, item, fireDelay, baseSoundLevel, baseZoom, projectileType, id);
        }

        public Builder setId(@NotNull String id) {
            this.id = id;
            return this;
        }

        public Builder setSpreadFactor(float spreadFactor) {
            this.spreadFactor = spreadFactor;
            return this;
        }

        public Builder setNumProjectiles(int numProjectiles) {
            this.numProjectiles = numProjectiles;
            return this;
        }

        public Builder setHorizontalRecoil(float horizontalRecoil) {
            this.horizontalRecoil = horizontalRecoil;
            return this;
        }

        public Builder setVerticalRecoil(float verticalRecoil) {
            this.verticalRecoil = verticalRecoil;
            return this;
        }
    }

}
