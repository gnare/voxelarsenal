package com.voxelbuster.voxelarsenal.objects;

import com.voxelbuster.voxelarsenal.VoxelArsenalPlugin;
import org.bukkit.NamespacedKey;

public class ItemDataUtil {
    private static final VoxelArsenalPlugin plugin = VoxelArsenalPlugin.getInstance();

    public static final NamespacedKey ammoKey = new NamespacedKey(plugin, "assets/ammo");
    public static final NamespacedKey maxAmmoKey = new NamespacedKey(plugin, "maxAmmo");
    public static final NamespacedKey ammoTypeKey = new NamespacedKey(plugin, "ammoType");
    public static final NamespacedKey weaponIdKey = new NamespacedKey(plugin, "weaponId");

    public static final float convertMetersPerSecond(float mps) {
        return mps / 20f;
    }
}
