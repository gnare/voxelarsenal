package com.voxelbuster.voxelarsenal.objects;

public class WeaponAttachment {
    private final String name;
    private final EnumWeaponAttachmentType type;
    private final EnumWeaponType[] supportedWeaponTypes;

    private final int ammo;
    private final float zoomFactor, soundMultiplier, recoilMultiplier, velocityMultiplier, spreadMultiplier,
            reloadTime, adsTime;

    public WeaponAttachment(String name, EnumWeaponAttachmentType type, EnumWeaponType[] supportedWeaponTypes,
                            int ammo, float zoomFactor, float soundMultiplier, float recoilMultiplier,
                            float velocityMultiplier,
                            float spreadMultiplier, float reloadTime, float adsTime) {
        this.name = name;
        this.type = type;
        this.supportedWeaponTypes = supportedWeaponTypes;
        this.ammo = ammo;
        this.zoomFactor = zoomFactor;
        this.soundMultiplier = soundMultiplier;
        this.recoilMultiplier = recoilMultiplier;
        this.velocityMultiplier = velocityMultiplier;
        this.spreadMultiplier = spreadMultiplier;
        this.reloadTime = reloadTime;
        this.adsTime = adsTime;
    }

    public String getDisplayName() {
        return this.name;
    }

    public float getZoomFactor() {
        return zoomFactor;
    }

    public float getRecoilMultiplier() {
        return recoilMultiplier;
    }

    public float getVelocityMultiplier() {
        return velocityMultiplier;
    }

    public float getSpreadMultiplier() {
        return spreadMultiplier;
    }

    public float getReloadTime() {
        return reloadTime;
    }

    public float getAdsTime() {
        return adsTime;
    }

    public int getAmmo() {
        return ammo;
    }

    public EnumWeaponAttachmentType getType() {
        return type;
    }

    public EnumWeaponType[] getSupportedWeaponTypes() {
        return supportedWeaponTypes;
    }

    public float getSoundMultiplier() {
        return soundMultiplier;
    }

    public static class Builder {

        private String name = "Attachment";
        private EnumWeaponAttachmentType type;
        private EnumWeaponType[] supportedWeaponTypes;
        private int ammo = 0;
        private float zoomFactor = 1f;
        private float soundMultiplier = 1f;
        private float recoilMultiplier = 1f;
        private float velocityMultiplier = 1f;
        private float spreadMultiplier = 1f;
        private float reloadTime = 0f;
        private float adsTime = 0f;

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setType(EnumWeaponAttachmentType type) {
            this.type = type;
            return this;
        }

        public Builder setSupportedWeaponTypes(EnumWeaponType[] supportedWeaponTypes) {
            this.supportedWeaponTypes = supportedWeaponTypes;
            return this;
        }

        public Builder setAmmo(int ammo) {
            this.ammo = ammo;
            return this;
        }

        public Builder setZoomFactor(float zoomFactor) {
            this.zoomFactor = zoomFactor;
            return this;
        }

        public Builder setSoundMultiplier(float soundMultiplier) {
            this.soundMultiplier = soundMultiplier;
            return this;
        }

        public Builder setRecoilMultiplier(float recoilMultiplier) {
            this.recoilMultiplier = recoilMultiplier;
            return this;
        }

        public Builder setVelocityMultiplier(float velocityMultiplier) {
            this.velocityMultiplier = velocityMultiplier;
            return this;
        }

        public Builder setSpreadMultiplier(float spreadMultiplier) {
            this.spreadMultiplier = spreadMultiplier;
            return this;
        }

        public Builder setReloadTime(float reloadTime) {
            this.reloadTime = reloadTime;
            return this;
        }

        public Builder setAdsTime(float adsTime) {
            this.adsTime = adsTime;
            return this;
        }

        public WeaponAttachment build() {
            return new WeaponAttachment(name, type, supportedWeaponTypes, ammo, zoomFactor, soundMultiplier,
                    recoilMultiplier, velocityMultiplier, spreadMultiplier, reloadTime, adsTime);
        }
    }
}
