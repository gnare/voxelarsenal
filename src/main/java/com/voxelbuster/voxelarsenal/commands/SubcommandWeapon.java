package com.voxelbuster.voxelarsenal.commands;

import com.voxelbuster.voxelarsenal.PermissionUtil;
import com.voxelbuster.voxelarsenal.VoxelArsenalPlugin;
import com.voxelbuster.voxelarsenal.WeaponManager;
import com.voxelbuster.voxelarsenal.objects.Weapon;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class SubcommandWeapon extends PluginSubcommand {
    private final List<String> scOptions;

    protected SubcommandWeapon(VoxelArsenalPlugin plugin) {
        super("weapon", plugin);
        this.aliases = Arrays.asList("weapon", "w");
        this.description = "Subcommand for weapon options";
        this.usage = "/va weapon <give/list/reload/setAmmo> " + ChatColor.YELLOW + "[args]";
        this.scOptions = Arrays.asList("give", "list", "reload", "setAmmo");
    }

    @Override
    public boolean execute(CommandSender sender, String alias, String[] args) {
        Weapon[] wlist = WeaponManager.getInstance().getAllWeapons();
        if (args.length == 2 && args[0].equalsIgnoreCase("give")) {
            if (PermissionUtil.hasPermission(sender, "voxelarsenal.weapon.give")) {
                if (sender instanceof Player) {
                    for (Weapon w : wlist) {
                        if (args[1].equalsIgnoreCase(w.getId())) {
                            Player p = (Player) sender;
                            p.getInventory().addItem(w.getItemStack());
                            return true;
                        }
                    }
                    sender.sendMessage(ChatColor.RED + "No such weapon.");
                    return false;
                } else {
                    sender.sendMessage(ChatColor.RED + "You cannot use this from the console!");
                    return false;
                }
            } else {
                sender.sendMessage(ChatColor.DARK_RED + "You do not have permission to use that command.");
                return false;
            }
        } else if (args.length == 1 && args[0].equalsIgnoreCase("list")) {
            if (PermissionUtil.hasPermission(sender, "voxelarsenal.weapon.list")) {
                for (Weapon w : wlist) {
                    StringBuilder messageBuilder = new StringBuilder();
                    messageBuilder.append(ChatColor.GREEN + w.getId());
                    messageBuilder.append(ChatColor.WHITE + " : ");
                    messageBuilder.append(ChatColor.GOLD + w.getDisplayName());
                    messageBuilder.append(ChatColor.WHITE + " : ");
                    messageBuilder.append(ChatColor.AQUA + WeaponManager.getInstance()
                            .getAmmoById(w.getAmmoID()).getDisplayName());
                    sender.sendMessage(messageBuilder.toString());
                }
                return true;
            } else {
                sender.sendMessage(ChatColor.DARK_RED + "You do not have permission to use that command.");
                return false;
            }
        } else if (args.length == 1 && args[0].equalsIgnoreCase("reload")) {
            if (PermissionUtil.hasPermission(sender, "voxelarsenal.weapon.reload")) {
                if (sender instanceof Player) {
                    Player player = (Player) sender;
                    ItemStack is = player.getInventory().getItemInMainHand();
                    Weapon w = Weapon.fromItemStack(is);
                    if (w == null) {
                        sender.sendMessage(ChatColor.RED + "Item is not a valid weapon.");
                        return false;
                    }
                    w.reload(player);
                    return true;
                } else {
                    sender.sendMessage(ChatColor.RED + "You must be a player to use this command!");
                    return false;
                }
            } else {
                sender.sendMessage(ChatColor.DARK_RED + "You do not have permission to use that command.");
                return false;
            }
        } else if (args.length == 2 && args[0].equalsIgnoreCase("setammo")) {
            if (PermissionUtil.hasPermission(sender, "voxelarsenal.weapon.setAmmo")) {
                if (sender instanceof Player) {
                    Player player = (Player) sender;
                    ItemStack is = player.getInventory().getItemInMainHand();
                    Weapon w = Weapon.fromItemStack(is);
                    if (w == null) {
                        sender.sendMessage(ChatColor.RED + "Item is not a valid weapon.");
                        return false;
                    }
                    w.setAmmo(Integer.parseInt(args[1]));
                    return true;
                } else {
                    sender.sendMessage(ChatColor.RED + "You must be a player to use this command!");
                    return false;
                }
            } else {
                sender.sendMessage(ChatColor.DARK_RED + "You do not have permission to use that command.");
                return false;
            }
        } else {
            sendUsageMessage(sender);
            return false;
        }
    }

    @Override
    public List<String> tabComplete(CommandSender sender, String alias, String[] args) throws IllegalArgumentException {
        Weapon[] wlist = WeaponManager.getInstance().getAllWeapons();
        if (args.length >= 1 && args[0].equalsIgnoreCase("give")) {
            if (PermissionUtil.hasPermission(sender, "voxelarsenal.weapon.give")) {
                if (sender instanceof Player) {
                    return Arrays.stream(wlist).map(w -> w.getId())
                            .filter(wid -> (args.length > 1) && wid.toLowerCase().contains(args[1].toLowerCase()))
                            .collect(Collectors.toList());
                }
            } else {
                return Collections.emptyList();
            }
        }

        return scOptions.stream().filter(s -> (args.length > 0) &&
                s.toLowerCase().contains(args[0].toLowerCase())).collect(Collectors.toList());
    }
}
