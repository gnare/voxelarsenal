package com.voxelbuster.voxelarsenal.commands;

import com.voxelbuster.voxelarsenal.PermissionUtil;
import com.voxelbuster.voxelarsenal.VoxelArsenalPlugin;
import com.voxelbuster.voxelarsenal.WeaponManager;
import com.voxelbuster.voxelarsenal.objects.Ammo;
import com.voxelbuster.voxelarsenal.objects.ItemDataUtil;
import com.voxelbuster.voxelarsenal.objects.Weapon;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.persistence.PersistentDataType;

import java.util.*;
import java.util.stream.Collectors;

public class SubcommandAmmo extends PluginSubcommand {
    private final List<String> scOptions;

    protected SubcommandAmmo(VoxelArsenalPlugin plugin) {
        super("weapon", plugin);
        this.aliases = Arrays.asList("ammo", "a");
        this.description = "Subcommand for ammo options";
        this.usage = "/va ammo <give/list/weapons> " + ChatColor.YELLOW + "[args]";
        this.scOptions = Arrays.asList("give", "list", "weapons");
    }

    @SuppressWarnings("DuplicatedCode")
    @Override
    public boolean execute(CommandSender sender, String alias, String[] args) {
        Ammo[] alist = WeaponManager.getInstance().getAllAmmo();
        if (args.length == 3 && args[0].equalsIgnoreCase("give")) {
            if (PermissionUtil.hasPermission(sender, "voxelarsenal.ammo.give")) {
                if (sender instanceof Player) {
                    if (!args[1].equalsIgnoreCase("hand")) {
                        for (Ammo a : alist) {
                            if (args[1].equalsIgnoreCase(a.getId())) {
                                Player player = (Player) sender;
                                player.getInventory().addItem(a.getItemStack(Integer.parseInt(args[2])));
                                return true;
                            }
                        }
                    } else if (args[1].equalsIgnoreCase("hand")) {
                        Player player = (Player) sender;
                        ItemStack is = player.getInventory().getItemInMainHand();
                        Weapon w = Weapon.fromItemStack(is);
                        Ammo a = WeaponManager.getInstance().getAmmoById(w.getAmmoID());
                        if (a != null) {
                            player.getInventory().addItem(a.getItemStack(Integer.parseInt(args[2])));
                        }
                    }
                    sender.sendMessage(ChatColor.RED + "No such ammo.");
                } else {
                    sender.sendMessage(ChatColor.RED + "You cannot use this from the console!");
                }
            } else {
                sender.sendMessage(ChatColor.DARK_RED + "You do not have permission to use that command.");
            }
            return false;
        } else if (args.length == 1 && args[0].equalsIgnoreCase("list")) {
            if (PermissionUtil.hasPermission(sender, "voxelarsenal.ammo.list")) {
                for (Ammo a : alist) {
                    StringBuilder messageBuilder = new StringBuilder();
                    messageBuilder.append(ChatColor.GREEN + a.getId());
                    messageBuilder.append(ChatColor.WHITE + " : ");
                    messageBuilder.append(ChatColor.GOLD + a.getDisplayName());
                    sender.sendMessage(messageBuilder.toString());
                }
                return true;
            } else {
                sender.sendMessage(ChatColor.DARK_RED + "You do not have permission to use that command.");
                return false;
            }
        } else if (args.length == 1 && args[0].equalsIgnoreCase("weapons")) {
            if (PermissionUtil.hasPermission(sender, "voxelarsenal.ammo.weapons")) {
                if (sender instanceof Player) {
                    HashSet<Weapon> matches = new HashSet<>();
                    Player player = (Player) sender;
                    ItemStack is = player.getInventory().getItemInMainHand();
                    if (is.getType() == Material.AIR) {
                        sender.sendMessage(ChatColor.RED + "Not valid ammo.");
                        return false;
                    }
                    String ammoId = is.getItemMeta().getPersistentDataContainer()
                            .get(ItemDataUtil.ammoTypeKey, PersistentDataType.STRING);
                    if (ammoId == null || ammoId.equals("")) {
                        sender.sendMessage(ChatColor.RED + "Not valid ammo.");
                        return false;
                    }
                    for (Weapon w : WeaponManager.getInstance().getAllWeapons()) {
                        if (w.getAmmoID().equals(ammoId)) {
                            matches.add(w);
                        }
                    }

                    if (matches.isEmpty()) {
                        sender.sendMessage(ChatColor.DARK_RED + "No weapons with ammo type " + ammoId);
                        return true;
                    }

                    for (Weapon w : matches) {
                        String messageBuilder = ChatColor.GREEN +
                                w.getId() +
                                ChatColor.WHITE + " : " +
                                ChatColor.GOLD + w.getDisplayName() +
                                ChatColor.WHITE + " : " +
                                ChatColor.AQUA + WeaponManager.getInstance()
                                .getAmmoById(w.getAmmoID()).getDisplayName();
                        sender.sendMessage(messageBuilder);
                    }
                    return true;
                } else {
                    sender.sendMessage(ChatColor.RED + "You must be a player to use this command!");
                    return false;
                }
            } else {
                sender.sendMessage(ChatColor.DARK_RED + "You do not have permission to use that command.");
                return false;
            }
        } else if (args.length == 2 && args[0].equalsIgnoreCase("weapons")) {
            if (PermissionUtil.hasPermission(sender, "voxelarsenal.ammo.weapons")) {
                HashSet<Weapon> matches = new HashSet<>();
                String ammoId = args[1];
                if (ammoId == null || ammoId.equals("")) {
                    sender.sendMessage(ChatColor.RED + "Not valid ammo.");
                    return false;
                }

                for (Weapon w : WeaponManager.getInstance().getAllWeapons()) {
                    if (w.getAmmoID().equals(ammoId)) {
                        matches.add(w);
                    }
                }

                if (matches.isEmpty()) {
                    sender.sendMessage(ChatColor.DARK_RED + "No weapons with ammo type " + ammoId);
                    return true;
                }

                for (Weapon w : matches) {
                    String messageBuilder = ChatColor.GREEN +
                            w.getId() +
                            ChatColor.WHITE + " : " +
                            ChatColor.GOLD + w.getDisplayName() +
                            ChatColor.WHITE + " : " +
                            ChatColor.AQUA + WeaponManager.getInstance()
                            .getAmmoById(w.getAmmoID()).getDisplayName();
                    sender.sendMessage(messageBuilder);
                }
                return true;
            } else {
                sender.sendMessage(ChatColor.DARK_RED + "You do not have permission to use that command.");
                return false;
            }
        } else {
            sendUsageMessage(sender);
            return false;
        }
    }

    @Override
    public List<String> tabComplete(CommandSender sender, String alias, String[] args) throws IllegalArgumentException {
        Weapon[] wlist = WeaponManager.getInstance().getAllWeapons();
        if (args.length >= 1 && args[0].equalsIgnoreCase("give")) {
            if (PermissionUtil.hasPermission(sender, "voxelarsenal.weapon.give")) {
                if (sender instanceof Player) {
                    return Arrays.stream(wlist).map(Weapon::getId)
                            .filter(wid -> (args.length > 1) && wid.toLowerCase().contains(args[1].toLowerCase()))
                            .collect(Collectors.toList());
                } else {
                    return Collections.emptyList();
                }
            } else {
                return Collections.emptyList();
            }
        } else if (args.length >= 1 && args[0].equalsIgnoreCase("weapons")) {
            return Arrays.stream(wlist).filter(w -> w.getAmmoID().toLowerCase().contains(args[1].toLowerCase()))
                    .map(Weapon::getAmmoID).collect(Collectors.toList());
        } else {
            return scOptions.stream().filter(s -> (args.length > 0) &&
                    s.toLowerCase().contains(args[0].toLowerCase())).collect(Collectors.toList());
        }
    }
}
