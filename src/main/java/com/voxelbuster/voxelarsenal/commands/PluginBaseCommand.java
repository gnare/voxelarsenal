package com.voxelbuster.voxelarsenal.commands;

import com.voxelbuster.voxelarsenal.VoxelArsenalPlugin;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class PluginBaseCommand extends Command implements CommandExecutor, TabCompleter {

    private final VoxelArsenalPlugin plugin;

    public PluginBaseCommand(VoxelArsenalPlugin plugin) {
        super("voxelarsenal");
        this.setAliases(Arrays.asList("voxelarsenal", "va", "varsenal"));
        this.description = ChatColor.AQUA +
                "Command that contains all functionality of VoxelArsenal" + ChatColor.GREEN + " /va" + ChatColor.AQUA
                + " or" + ChatColor.GREEN + " /va help to see subcommands.";
        this.usageMessage = ChatColor.GREEN + "/va <subcommand>";
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String alias, String[] args) {
        return execute(commandSender, alias, args);
    }

    @Override
    public boolean execute(CommandSender commandSender, String alias, String[] args) {
        if (getAliases().contains(alias.toLowerCase())) {
            if (args.length > 0) {
                SubCommand sc = SubCommand.subCommandByAlias(args[0].toLowerCase());
                if (sc == null) {
                    return new SubcommandHelp(plugin).execute(commandSender, "help", new String[0]);
                }
                switch (sc) {
                    case NONE:
                    case HELP:
                        return new SubcommandHelp(plugin)
                                .execute(commandSender, alias, Arrays.copyOfRange(args, 1, args.length));
                    case WEAPON:
                        return new SubcommandWeapon(plugin)
                                .execute(commandSender, alias, Arrays.copyOfRange(args, 1, args.length));
                    case AMMO:
                        return new SubcommandAmmo(plugin)
                                .execute(commandSender, alias, Arrays.copyOfRange(args, 1, args.length));
                    case RELOAD:
                        return new SubcommandReload(plugin)
                                .execute(commandSender, alias, Arrays.copyOfRange(args, 1, args.length));
                    default:
                        commandSender.sendMessage(ChatColor.RED + "Invalid subcommand. " + ChatColor.GREEN +
                                "/hl help " + ChatColor.RED + "for help.");
                        return false;
                }
            } else {
                return new SubcommandHelp(plugin).execute(commandSender, "help", new String[0]);
            }
        }
        return false;
    }

    @Override
    public List<String> tabComplete(CommandSender commandSender, String alias,
                                    String[] args) throws IllegalArgumentException {
        if (getAliases().contains(alias.toLowerCase())) {
            if (args.length == 1) {
                return SubCommand.getAllAliases().stream().filter(s -> s.startsWith(args[0].toLowerCase()))
                        .collect(Collectors.toList());
            } else {
                SubCommand sc = SubCommand.subCommandByAlias(args[0].toLowerCase());
                if (sc == null) {
                    return new ArrayList<>();
                }
                switch (sc) {
                    case NONE:
                        return new ArrayList<>();
                    case HELP:
                        return new SubcommandHelp(plugin)
                                .tabComplete(commandSender, alias, Arrays.copyOfRange(args, 1, args.length));
                    case WEAPON:
                        return new SubcommandWeapon(plugin)
                                .tabComplete(commandSender, alias, Arrays.copyOfRange(args, 1, args.length));
                    case AMMO:
                        return new SubcommandAmmo(plugin)
                                .tabComplete(commandSender, alias, Arrays.copyOfRange(args, 1, args.length));
                    case RELOAD:
                        return new SubcommandReload(plugin)
                                .tabComplete(commandSender, alias, Arrays.copyOfRange(args, 1, args.length));
                    default:
                        return super.tabComplete(commandSender, alias, args);
                }
            }
        } else {
            return super.tabComplete(commandSender, alias, args);
        }
    }

    @Override
    public List<String> onTabComplete(CommandSender commandSender, Command command, String alias, String[] args) {
        return tabComplete(commandSender, alias, args);
    }


    enum SubCommand {
        NONE(SubcommandHelp.class, "", null),
        HELP(SubcommandHelp.class, "help", "h"),
        WEAPON(SubcommandWeapon.class, "weapon", "w"),
        AMMO(SubcommandAmmo.class, "ammo", "a"),
        RELOAD(SubcommandReload.class, "reload");

        private final List<String> aliases;

        private final Class<? extends PluginSubcommand> pluginSubcommandClass;

        SubCommand(Class<? extends PluginSubcommand> pluginSubcommandClass,
                   String... aliases) {
            this.pluginSubcommandClass = pluginSubcommandClass;
            this.aliases = Arrays.asList(aliases);
        }

        public static SubCommand subCommandByAlias(String s) {
            for (SubCommand subCommand : values()) {
                if (subCommand.hasAlias(s)) {
                    return subCommand;
                }
            }
            return null;
        }

        public boolean hasAlias(String s) {
            return this.aliases.contains(s);
        }

        public static ArrayList<String> getAllAliases() {
            ArrayList<String> aliases = new ArrayList<>();
            for (SubCommand subCommand : values()) {
                aliases.addAll(subCommand.aliases.stream().filter(s -> s != null && !s.equals(""))
                        .collect(Collectors.toCollection(ArrayList::new)));
            }
            return aliases;
        }

        public Class<? extends PluginSubcommand> getSubcommand() {
            return pluginSubcommandClass;
        }
    }
}
