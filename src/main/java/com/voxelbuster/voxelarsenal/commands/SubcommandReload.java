package com.voxelbuster.voxelarsenal.commands;

import com.voxelbuster.voxelarsenal.PermissionUtil;
import com.voxelbuster.voxelarsenal.VoxelArsenalPlugin;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.util.Collections;
import java.util.List;

public class SubcommandReload extends PluginSubcommand {
    protected SubcommandReload(VoxelArsenalPlugin plugin) {
        super("reload", plugin);
        this.description = "Reloads the plugin and player data from the config.";
        this.usage = "/va reload";
        this.aliases = Collections.singletonList("reload");
    }

    @Override
    public boolean execute(CommandSender sender, String alias, String[] args) {
        if (PermissionUtil.hasPermission(sender, "voxelarsenal.reload")) {
            plugin.reload();

            sender.sendMessage(ChatColor.GREEN + "VoxelArsenal reload complete.");
            return true;
        } else {
            sender.sendMessage(ChatColor.DARK_RED + "You do not have permission to use that command.");
            return false;
        }
    }

    @Override
    public List<String> tabComplete(CommandSender sender, String alias, String[] args) throws IllegalArgumentException {
        return Collections.emptyList();
    }

}
