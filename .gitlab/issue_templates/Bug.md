<!--- Credit https://github.com/stevemao/github-issue-templates/blob/master/bugs-only/ISSUE_TEMPLATE.md-->

Issue tracker is **ONLY** used for reporting bugs. New features should be discussed on spigotmc.org. Please use [stackoverflow](https://stackoverflow.com) for supporting issues.

<!--- Provide a general summary of the issue in the Title above -->

## Expected Behavior
<!--- Tell us what should happen -->

## Current Behavior
<!--- Tell us what happens instead of the expected behavior -->

## Possible Solution
<!--- Not obligatory, but suggest a fix/reason for the bug, -->

## Steps to Reproduce
<!--- Provide a link to a live example, or an unambiguous set of steps to -->
<!--- reproduce this bug. Include code to reproduce, if relevant -->
1.
2.
3.
4.

<!--- Provide a general summary of the issue in the Title above -->

**PLEASE INCLUDE A LINK TO SERVER LOGS IF POSSIBLE**

## Detailed Description
<!--- Provide a detailed description of the change or addition you are proposing -->

## Possible Implementation
<!--- Not obligatory, but suggest an idea for implementing addition or change -->