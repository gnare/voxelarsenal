# VoxelArsenal

A Spigot plugin that adds weapons and ballistics to the game.

Use the weapon JSON files to manage the available weapon and ammo items. Also includes custom events for certain actions.
Javadocs are included for such events.

#### Commands:

*Note:* All commands are tab-completed, so they will display valid options where applicable.

`/va` - Show Help

`/va help` - Show Help

`/va weapon <give|list|setAmmo|reload> [args]` - Commands to control and get info about weapon items.

`/va ammo <give|list|weapons> [args]` - Commands to control and get info about ammo items.

`/va reload` - Reload the plugin from config

------

#### Config:
`explosionsDestroyBlocks` : *Boolean* - True if explosions from Rocket projectiles destroy blocks.

`armorReducesBulletDamage` : *Boolean* - True if bullets do not ignore armor.

`globalAllowHeadshots` : *Boolean* - True if headshots are enabled

`globalHeadshotMultiplier` : *Float* - The value damage is multiplied by if a headshot is hit.

`dropShellCasings` : *Boolean* - True if firing a weapon drops "casings"; unrecoverable, short-lived
items designated by `shellCasingItemId`.

`requireAmmoItemForReload` : *Boolean* - True if the player must have the ammo item in their inventory
in order to reload. Ignored in creative.

`weaponStartWithFullAmmo` : *Boolean* - True if weapons given via the `/va weapon give` command start with a full magazine

`shellCasingItemId` : *String* - The full namespaced item ID of the item dropped as "casings".

`bulletproofEntities` : *List* - A list of Entity Types that this plugin will not damage with bullets.

------

#### Weapon Config:

There are 5 weapon and 5 ammo configuration JSON files included with the plugin that control the behavior of the plugin's weapons.
You may change or add to them as you see fit.

***NOTE***: All config values are required for new weapons and ammo. If a weapon or ammo config is missing any values it
will not work.

Weapon Options:
`damage` : *Float* - Half-hearts of damage to deal when a shot hits. This applies for each pellet of multi-projectile
weapons.

`projectileVelocity` : *Float* - Speed of the launched projectile (if applicable, you can set this to anything if hitscan).
For reference, 4.0 is about the speed of an arrow (~80m/s).

`reloadTime` : *Float* - The time in seconds required to reload.

`adsTime` : *Float* - The time in seconds it takes to toggle zoom on the weapon.

`fireDelay` : *Float* - The minimum time in seconds between shots.

`baseSoundLevel` : *Float* - The distance that a shot can be heard from. (1.0 = ~16 meters)

`baseZoom` : *Float* - The approximate FOV ratio when the weapon is ADSed.

`spreadFactor` : *Float* - The maximum random spread (in degrees) of projectiles created by the weapon. Set this to 0 to make
projectiles always fire in the exact direction of the crosshair.

`horizontalRecoil`/`verticalRecoil` : *Float* - Arbitrary amount the player's view is moved after firing. Currently does not do anything.

`ammoMaximum` : *Integer* - Maximum amount of ammo the weapon can hold.

`attachmentMaximum` : *Integer* - Set to 0, currently unused.

`numProjectiles` : *Integer* - Number of projectiles launched in a single shot.

`ammoID` : *String* - The ID of the ammo JSON that this weapon uses.
 
`weaponType` : *String* - Any of PISTOL, REVOLVER, SHOTGUN, BOLT_SNIPER, SEMI_AUTO_SNIPER,
ASSAULT_RIFLE, SMG, LMG, GRENADE_LAUNCHER, ROCKET_LAUNCHER

`projectileType` : *String* - Any of ROCKET, BULLET, or HITSCAN. HITSCAN is currently experimental. Use at own risk.
BULLET uses an arrow to simulate actual ballistics. ROCKET shoots an explosive fireball.

`displayName` : *String* - Display name of the weapon item.

`id` : *String* - The weapon ID. Must not be the same as any other weapons.

`item` : *String* - The Minecraft item used as the weapon. Have a look at the Javadoc for org.bukkit.Material if unsure.

#### Ammo Config:

`id` : *String* - The ammo ID. Must not be the same as any other ammo.

`displayName` : *String* - Display name of the ammo item.

`itemId` : *String* - The namespaced item ID used as the ammo item.

`explosive` : *Boolean* - True if the ammo explodes on bullet hit.

`explosionDestroysBlocks` : *Boolean* - True if the explosions destroy blocks.

`explosionPower` : *Float* - The power of the explosion created by the projectile. 1 ~= TNT

------

#### Permissions
 - voxelarsenal.reload
 - voxelarsenal.weapon.give
 - voxelarsenal.weapon.list
 - voxelarsenal.weapon.reload
 - voxelarsenal.weapon.setAmmo
 - voxelarsenal.weapon.freeReload
 - voxelarsenal.ammo.give
 - voxelarsenal.ammo.list
 - voxelarsenal.ammo.weapons

------

#### Other info:
 