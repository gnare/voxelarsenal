#!/usr/bin/env bash

echo "Creating .genResources file"

TARGET_DIR=./test_server/plugins/VoxelArsenal
TARGET_PATH=$TARGET_DIR/.genResources

mkdirs "$TARGET_DIR"
touch "$TARGET_PATH"