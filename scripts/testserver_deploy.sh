#!/usr/bin/env bash
echo "Deploying PaperSpigot Test Server"
mkdir ./test_server

PAPER_JAR=./test_server/paper.jar
EULA=./test_server/eula.txt
START=./test_server/start.sh
START_WIN=./test_server/start.bat
DEBUG_WIN=./test_server/debug.bat
DEBUG=./test_server/debug.sh
PAPER_URL=https://papermc.io/api/v1/paper/1.15.2/latest/download

if test ! -f "$PAPER_JAR"; then
  echo "Downloading latest Paper JAR"
  wget -O "$PAPER_JAR" "$PAPER_URL"
  echo "eula=true" >> "$EULA"
fi

echo "Copying scripts"
cp ./scripts/start.sh "$START"
cp ./scripts/debug.sh "$DEBUG"
cp ./scripts/start.bat "$START_WIN"
cp ./scripts/debug.bat "$DEBUG_WIN"

echo "Done"
