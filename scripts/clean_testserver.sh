#!/usr/bin/env bash
echo "Cleaning test server"

TEST_SERVER_DIR=./test_server

rm -rf "$TEST_SERVER_DIR"
echo "Done"