#!/usr/bin/env bash

echo "Copying generated resources."

SRC_ROOT=./test_server/plugins/VoxelArsenal
SRC_GLOB="$SRC_ROOT/ammo $SRC_ROOT/weapons"
DEST_DIR=./src/main/resources/

cp -R "$SRC_GLOB" "$DEST_DIR"