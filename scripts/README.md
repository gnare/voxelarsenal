## /scripts/ readme

The scripts in this folder are designed to be used in conjunction 
with the IntelliJ run configurations for automatic test server deployment, 
and the build tree.

These scripts do not run natively on Windows, and are not tested on Mac. 
If you would like to use these scripts on Windows, the use of Cygwin with 
`bash` or WSL is advised.

**Note:** Having your test server directory listed in the `.gitignore` is
highly recommended.

#### IntelliJ Build Tree:
Remote Debug → Start Test Server → Deploy Plugins/Test Server → Maven `install`

Clean all → Clean Test Server → Maven `clean`